package com.example.listview;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;
import android.app.Activity;

import androidx.core.widget.ListViewAutoScrollHelper;

public class MainActivity extends Activity
{

    ListView listView;
    String[] web =
            {
                    "Nguyễn Thị Thu Uyên",
                    "Anish Azaf",
                    "Aro Norman",
                    "Bealsen",
                    "Bealse",
                    "Branklin",
                    "Banie"
            };
    Integer[] imageId =
            {
                    R.drawable.image2,
                    R.drawable.java,
                    R.drawable.cauturv,
                    R.drawable.giaitich,
                    R.drawable.daso,
                    R.drawable.toan,
                    R.drawable.li


            };
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        CustomList adapter = new CustomList(MainActivity.this, web, imageId);

        listView= (ListView) findViewById(R.id.list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override

            public void onItemClick(AdapterView <? > parent, View view,
                                    int position, long id)
            {

                Intent intent =new Intent(MainActivity.this,display.class);
                startActivity(intent);
            }


        });

    }


}